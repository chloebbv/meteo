'use strict';


function getMeteo() {
    let ville ='';
    document.querySelector('#result').classList.add('hide');
    ville = $('input').val();
    ville = ville.replace(/ /g, "");
    let url = "https://api.openweathermap.org/data/2.5/weather?";
    url += "q=" + ville;
    url += "&appid=c21a75b667d6f7abb81f118dcf8d4611";
    url += "&units=metric";
    url += "&lang=fr";
    url += "&ctn=3";
    $.ajax({
        url: url,
        datatype: 'json',
        success : displayMeteo})
}

function displayMeteo(datas) {
    console.log(datas);
    document.querySelector('#result').classList.remove('hide');
    $('article h2 strong').text(datas.name);
    $('article h2 sup').text(datas.sys.country);
    $('article p strong').text(parseInt(datas.main.temp));
    $('article small').text(datas.weather[0].description);
    $('article img').attr('src', `img/${datas.weather[0].icon}.png`);
    $('article').fadeIn();
}

$(document).ready(function() {
    $('button').on('click', getMeteo)
})